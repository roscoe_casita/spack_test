# Copyright 2013-2019 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install lock-free-structs
#
# You can edit this file again by typing:
#
#     spack edit lock-free-structs
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class LockFreeStructs(Package):
    """FIXME: Put a proper description of your package here."""

    # FIXME: Add a proper url for your package's homepage here.
    homepage = "http://www.example.com"
    url      = "https://bitbucket.org/roscoe_casita/lock_free_structs/raw/ee08f4bae25f4abe7f9ebfb52203f67a539eb462/release/download/v1.0/LockFreeStructs-1.0.tar.gz"

    version('1.0', sha256='ab4319ef494534ec3c06eea5011da05a7e5ac7dae9912b70110d4173eac9b39d')

    # FIXME: Add dependencies if required.
    # depends_on('foo')

    def install(self, spec, prefix):
        # FIXME: Unknown build system
        make()
        
        install('MPMC',prefix.bin)
