
SPACK_ROOT=spack

PATH=$PATH:$SPACK_ROOT/bin

source $SPACK_ROOT/share/spack/setup-env.sh

MPMCDIR=$(spack location --install-dir lock-free-structs)


$MPMCDIR/bin $1 $2 $3 $4
